========
birdshot
========


Utilities and functions specific to the BIRDSHOT project.

This package will most frequently be used as a command line tool within the
Carta Workflow framework (powered by SevenBridges). Subcommands to the
``birdshot`` CLI create and operate on a stream of objects, similar to a linux
pipe.

Subcommands can be broadly split into two categories: generators and
processors.

Generators add objects into the stream. ``load`` is an excellent example as it
reads an object from a file and adds that object to the stream. Multiple
``load`` commands will add multiple objects to the stream.

Processors act on the objects in the stream and can modify, consume, or let
pass stream objects.

For example, a graph previously downloaded from Google Drive could be loaded
and the nodes filtered to list all JSON files::

    birdshot load "graph.pkl" store -k G nodes node-filter -n '.*.json' path --graph G

will return the fully qualified path of all JSON files. Let's break that down
into the individual parts::

    birdshot load "graph.pkl"...

This adds the graph stored in "graph.pkl" into the stream. The stream now
contains one object: the graph. As we'll need it later, store a copy of this
graph::

    ... store -k G ...

This stores the graph on the heap: a map of named variables. This is
effectively equivalent to creating a key-value pair in a dictionary::

    var = {"G": graph}

And like a dictionary, all the variables must be given unique names. The graph
contains nodes and edges that define the graph structure.

The ``store`` command does not consume the graph. The graph continues through
the stream. However, we want to work with the nodes in the graph--the nodes
represent files and folders in the Birdshot Google Drive. To access these
nodes is a simple command::

    ... nodes ...

This consumes the graph (it is no longer in the stream) and adds all the nodes
to the stream. (You can think about the stream as a list or queue of objects
to be processed.) From there we want to find nodes whose name (``-n``) matches
a regular expression pattern::

    ... node-filter -n '.+\.json' ...

The ``-n`` option tests against the node name (e.g., the filename or folder
name). Only matching names are kept. All others are discarded. Alternatively,
one could search on node ID using the ``--id`` flag.

.. note::
    ``node-filter`` matches
    `regular expressions <https://docs.python.org/3/library/re.html>`_ not
    `glob pattern expansion <https://docs.python.org/3/library/glob.html>`_.
    Subtle and easy to miss, the pattern above, ``.+\.json`` matches any file
    ending in ".json". The analogous glob pattern, ``*.json``, while simplier
    for file names, does not extend to more general pattern matching.

Once matched, we want to list the path to each JSON file::

    ... path --graph G

This takes each node and, using the graph stored on the heap ``--graph G``,
returns the path.

Installation
============
This package relies on *pycarta* and *carta-integrations*, neither of which is
available through PyPi and must be installed through wheels. These two packages
and their dependencies are provided in the *wheelhouse* folder and should be
installed before pip-installing birdshot.

Start by compiling the wheels for the local packages in a *clean/fresh virtual environment*::

    python -m pip wheel -w wheelhouse /path/to/carta/carta-integrations
    python -m pip install --no-index --find-links=wheelhouse --force-reinstall carta-integrations

and, similarly, for pycarta::

    python -m pip wheel -w wheelhouse /path/to/pycarta
    python -m pip install --no-index --find-links=wheelhouse --force-reinstall pycarta


After this, install birdshot: ``pip install .``

It may be necessary to deactivate/reactivate the virtual environment before the
``birdshot`` CLI is accessible.

Building the Docker Container
-----------------------------

Once the package is built in a clean virtual environment: 

1. Delete all wheels in *wheelhouse/* except *carta-integrations* and *pycarta*.
2. ``pip freeze | sed -e 's/=.*//' > requirements.txt``
3. Edit the *requirements.txt* to remove birdshot, carta-integrations, and pycarta.
4. `docker buildx build --platform linux/x86_64 -t images.sbgenomics.com/<division>/birdshot:<major>.<minor>.<patch> .`

The *carta-integrations* and *pycarta* packages are pure python and, therefore,
are platform independent.

.. note::
    Step 4 can be used for custom container. Just replace the command above with::

        docker buildx build --platform linux/x86_64 -t images.sbgenomics.com/htmdec/<IMAGE NAME>:<IMAGE VERSION> .

Uploading Your Docker Container to Seven Bridges
------------------------------------------------

To push the newly built docker container into Seven Bridges and make it
available for building Apps requires some one-time setup. You must first login
to the Seven Bridges repo. For this you will need your Seven Bridges API key.

1. Log in to `Seven Bridges <https://igor.sbgenomics.com/home>`_
2. From the header bar, select *Developer* >> *Authentication token*. If you don't have a token, click *Generate Token*.
3. Copy the token. You'll need that for the next step: ``docker login``.

From a terminal on your local computer--e.g., Terminal on POSIX/MacOS or
PowerShell on Windows::

    docker login images.sbgenomics.com/<division> -u <YOUR USERNAME>

You will then be prompted for your password. This is the API token you copied
above. (Alternatively you can provide the token on the ``docker login``
command line with the ``-p <API KEY>`` option, but then your API key will be
visible in your terminal history. Generally this should be avoided.)

Push
^^^^

Having logged in you can now push your docker image to Seven Bridges::

    docker push images.sbgenomics.com/<division>/<IMAGE NAME>:<IMAGE VERSION>

Usage
=====
This package relies on two sets of credentials that should be maintained in
confidence:

*Carta username/password* If not specified, the environment will be searched
for CARTA_USER (username) and CARTA_PASS (password).


Note
====

This project has been set up using PyScaffold 4.5. For details and usage
information on PyScaffold see https://pyscaffold.org/.
