#!/usr/bin/env zsh

birdshot \
       load ../test-graph-retrieval/birdshot-20230914.pkl store -k G nodes \
         node-filter -n 'ni-srjt.*.json' push --pass \
           parents --graph G get -i 0 \
         children --graph G push --pass \
       apply -p "field=name|missing=exit" field \
         apply -p "pattern='HTMDEC.*Results.*'|ignore_case=False|missing='exit'" match \
           apply any get -i 0 \
         store -k mask drop \
       pop -i 0 where -s mask \
         path --graph G \
           push \
         pop -i 0 where -s mask \
       apply -p "id=[]|name=['HTMDEC.*Results.*']|default=None" node-filter get -i 0 \
         path --graph G \
           pop --zip \
         dumps

