from __future__ import annotations

import json
import logging
import networkx as nx
import requests
import warnings

from carta_integrations.google.drive import GDrive
from collections import namedtuple
from pathlib import Path
try:
    from pycarta.auth import AuthorizationAgent
    from pycarta.api import CartaObjectStorage
except:
    msg = """
        pycarta could not be imported. The version of pycarta currently
        published on PyPi is incompatible with this package. pycarta should be
        installed using a wheel installation from the most recent version.
        """
    logging.error(msg)
    raise ImportError(msg)
from typing import Any, Optional, Union


# This does not handle the recursive content checking
# needed for a full definition of JSON types.
JsonType = Union[str, float, int, None, list[Any], dict[str, Any]]


class Birdshot(GDrive):
    def __init__(self, username, password, *, url : str):
        # Log in to Carta
        agent = AuthorizationAgent(username, password, url=url)
        self.agent = agent
        # Retrieve Google Drive secret from dispatcher.
        credentials = self._get_google_drive_credentials()
        # Use the Google Drive credentials retrieved from Carta to setup
        # the Google Drive access agent.
        super().__init__(service_account_info=credentials)
        self.cartaObjectStorage = CartaObjectStorage(agent)
        # State variables
        self._graph = None
        self._root = namedtuple("EmptyRoot", "id")(None)
    
    def _get_google_drive_credentials(self) -> JsonType:
        """
        Retrieves the Google Drive credentials.
        """
        service_= lambda endpoint: f"service/birdshot/htmdec/{endpoint}"
        try:
            response = self.agent.get(service_("check"))
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            e.response.reason = f"No access to {self.agent.url}/{service_('')}."
            raise
        try:
            response = self.agent.get(service_(f"credentials/get?resource=google-drive"))
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            e.response.reason = f"Failed to access the BIRDSHOT resource credentials for Google Drive."
            raise
        return response.json()
    
    @property
    def project(self):
        # The BIRDSHOT project has only one project
        return self.cartaObjectStorage.projects[0]
    
    @property
    def connection(self):
        # The BIRDSHOT project has only one connection
        return self.cartaObjectStorage.connections[0][0]

    def build_graph(self, rootId=None):
        """
        Google Drive exposes a list function that is much faster than
        iterating through nodes sequentially.

        Note: This method does not change the state of the instance but,
        instead, is intended to build graphs from a given node ID. If you want
        to set the graph available in an instance graph, see
        `Birdshot.graph(nodeId)`.

        Parameters
        ----------
        rootId : str
            File ID of the root node in the graph. If not given, this is the
            project connection root
        
        Returns
        -------
        """
        rootId = rootId or self.connection.root.nativeId.fileId
        G = nx.DiGraph()
        # Get files (a.k.a. Nodes)
        FILES = self.gds.files()
        nextPageToken = None
        logging.debug("Downloading available files/folders from Google Drive.")
        while True:
            logging.debug(f"Fetching next page of entries using page token: {nextPageToken!r}.")
            # All files accessible with this API key are of interest.
            response = FILES.list(
                corpora="user",
                q=f"trashed = false",
                fields="*",
                includeItemsFromAllDrives=True,
                supportsAllDrives=True,
                pageToken=nextPageToken,
                pageSize=50).execute()
            # Add the set of nodes just read.
            G.add_nodes_from([
                Birdshot.Entry(id=f.pop("id"), metadata=f)
                for f in response.get("files", [])
            ])
            nextPageToken = response.get("nextPageToken", None)
            if not nextPageToken:
                break
        logging.info(f"Read {len(G)} files (nodes) from BIRDSHOT.")
        # Add the edges
        nodeMap = {n.id:n for n in G.nodes}
        # Build the edges from the nodes, starting at the root.
        logging.debug("Adding edges to graph.")
        try:
            queue = [rootId]
        except KeyError:
            msg = f"Did not find the project root ({rootId!r}) in Google Drive."
            logging.error(msg)
            raise KeyError(msg)
        while True:
            # Get parent
            try:
                parent = queue.pop()
            except IndexError:
                break
            children = [n.id for n in G.nodes
                        if parent in n.metadata.get("parents", [])]
            queue.extend(children)
            G.add_edges_from([
                (nodeMap[parent], nodeMap[child]) for child in children])
        # Create a new graph that keeps only those nodes that are connected to
        # the root
        result = nx.DiGraph()
        result.add_edges_from(G.edges)
        # Done
        logging.debug("Finished building graph.")
        return namedtuple("BuildGraph", "graph root")(result, nodeMap[rootId])

    @property
    def G(self):
        if self._graph is None:
            self._graph, self._root = self.build_graph()
        return self._graph
    
    @property
    def root(self):
        """
        Root node of the current graph.
        
        Note: this is the node document retrieved from Google and structured
        as a Node. The ID (e.g., "nativeId") of this node can be retrieved as
        `<Birdshot instance>.root.id`.
        """
        return self._root
    
    def graph(
        self,
        fileId : Union[str, Path, Birdshot.Entry]=None,
        graph : Optional[nx.DiGraph]=None
    ) -> nx.DiGraph:
        """
        Within Google Drive it is easier to build the graph by retrieving all
        available nodes and building the graph from the specified root.

        If `fileId` is given, this forces a rebuild of the graph. Therefore, to
        force a rebuild of the project graph:

            birdshot = Birdshot(<username>, <password>)
            ...
            birdshot.graph(birdshot.root.id)

        To access the graph itself, use `birdshot.G` (`birdshot` is a
        `Birdshot` instance, as above).

        Parameters
        ----------
        fileId : str | Path | Birdshot.Entry
            The root node. If not specified, use the root node for this project
            connection.
        graph : networkx.DiGraph
            Unnecessary in this function. Kept here to maintain API
            compatibility with the base class.
        
        Returns
        -------
        networkx.DiGraph
            The graph of the resource structure.
        """
        if fileId is not None:
            warnings.warn(f"Retrieving graph rooted at {fileId!r}. This may take a while.")
            self._graph, self._root = self.build_graph(fileId)
        return self.G
