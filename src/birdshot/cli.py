"""
BIRDSHOT project actions.
"""

import os
import click
# import argparse
import logging

from birdshot import __version__
from birdshot import Birdshot

__author__ = "Branden Kappes"
__copyright__ = "Branden Kappes"
__license__ = "MIT"


if "DEBUG" in os.environ:
    logging.basicConfig(level=logging.DEBUG)
if "INFO" in os.environ:
    logging.basicConfig(level=logging.INFO)
if "WARNING" in os.environ:
    logging.basicConfig(level=logging.WARNING)
if "ERROR" in os.environ:
    logging.basicConfig(level=logging.ERROR)
else:
    logging.basicConfig(level=logging.CRITICAL)

# logging.basicConfig(level=logging.DEBUG)


class UserNotFoundError(Exception):
    pass


class Storage:
    def __init__(self, username=None, password=None, url=None):
        self._heap = dict()
        self._stack = list()
        # Setup the birdshot object
        user = username or os.environ.get("CARTA_USER", None)
        passwd = password or os.environ.get("CARTA_PASS", None)
        url = url or os.environ.get("CARTA_API_URL", None)
        # Create birdshot
        if user and passwd:
            try:
                self.birdshot = Birdshot(user, passwd, url=url)
                logging.debug(f"Authenticated user (xxx{user[-3:]}) with password (xxx{user[-3:]}).")
            except Exception as e:
                msg = f"Failed to establish BIRDSHOT user {user!r}. Failed with message: {str(e)}"
                raise UserNotFoundError(msg)
        else:
            self.birdshot = None


@click.group(chain=True)
@click.option(
    "-u", "--username",
    type=str,
    default=None,
    help="Carta username. Default: looks for the username in the CARTA_USER environment variable.")
@click.option(
    "-p", "--password",
    type=str,
    default=None,
    help="Carta password. Default: Looks for the password in the CARTA_PASS environment variable.")
@click.option(
    "--url",
    type=click.Choice([
        "https://api.carta.contextualize.us.com",
        "https://api.sandbox.carta.contextualize.us.com"
    ]),
    default="https://api.carta.contextualize.us.com",
    help="Development Only. Base URL of the Carta API."
)
@click.pass_context
def cli(ctx, username, password, url):
    """
    BIRDSHOT object manipulation.
    
    This CLI allows users to read, write, and manipulate data stored in the
    BIRDSHOT project Google Drive from the command line.

    Logging can be enabled by setting DEBUG, INFO, WARNING, or ERROR
    environment variables.
    """
    ctx.obj = Storage(username, password, url)


@cli.result_callback()
def process_commands(processors, **kwds):
    """
    This result callback is invoked with an iterable of all the chained
    subcommands.  Each subcommand returns a function that is chained together
    to feed one into the other, similar to how a pipe on unix works.
    """
    # Start with an empty iterable.
    stream = ()

    # Pipe it through all stream processors.
    for processor in processors:
        stream = processor(stream)

    # Evaluate the stream and throw away the items.
    for _ in stream:
        pass


# Add commands
from .operations import (
    load, dump, dumps, store, field, get, iter_, push, pop, model_dump, drop,
    len, dict_, list_, where, any_, all_, strip, apply, bool_, str_, replace,
    graph, nodes, node_filter, path, download, parents, children
)
cli.command()(load) # tested
cli.command()(dump) # tested
cli.command()(dumps) # tested
cli.command()(store) # tested
cli.command()(field) # tested
cli.command()(get) # tested
cli.command("iter")(iter_) # tested
cli.command()(push) # tested
cli.command()(pop) # tested
cli.command("model-dump")(model_dump) # tested
cli.command()(drop) # tested
cli.command()(len) # tested
cli.command("dict")(dict_) # tested
cli.command()(where) # tested
cli.command("any")(any_) # tested
cli.command("all")(all_)
cli.command()(apply) # tested
cli.command("bool")(bool_) # tested
cli.command("str")(str_)
cli.command()(strip) # tested
cli.command()(replace) # tested
cli.command()(graph) # tested
cli.command()(nodes) # tested
cli.command("list")(list_) # tested
cli.command("node-filter")(node_filter) # tested
cli.command()(path) # tested
cli.command()(download) # tested
cli.command()(parents) # tested
cli.command()(children) # tested


# ---- Python API ----
# The functions defined in this section can be imported by users in their
# Python scripts/interactive interpreter, e.g. via
# `from birdshot.skeleton import fib`,
# when using this Python module as a library.


# ---- CLI ----
# The functions defined in this section are wrappers around the main Python
# API allowing them to be called directly from the terminal as a CLI
# executable/script.


# def parse_args(args):
#     """Parse command line parameters

#     Args:
#       args (List[str]): command line parameters as list of strings
#           (for example  ``["--help"]``).

#     Returns:
#       :obj:`argparse.Namespace`: command line parameters namespace
#     """
#     parser = argparse.ArgumentParser(description="Just a Fibonacci demonstration")
#     parser.add_argument(
#         "--version",
#         action="version",
#         version=f"birdshot {__version__}",
#     )
#     parser.add_argument(dest="n", help="n-th Fibonacci number", type=int, metavar="INT")
#     parser.add_argument(
#         "-v",
#         "--verbose",
#         dest="loglevel",
#         help="set loglevel to INFO",
#         action="store_const",
#         const=logging.INFO,
#     )
#     parser.add_argument(
#         "-vv",
#         "--very-verbose",
#         dest="loglevel",
#         help="set loglevel to DEBUG",
#         action="store_const",
#         const=logging.DEBUG,
#     )
#     return parser.parse_args(args)


# def setup_logging(loglevel):
#     """Setup basic logging

#     Args:
#       loglevel (int): minimum loglevel for emitting messages
#     """
#     logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
#     logging.basicConfig(
#         level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
#     )


# def main(args):
#     """Wrapper allowing :func:`fib` to be called with string arguments in a CLI fashion

#     Instead of returning the value from :func:`fib`, it prints the result to the
#     ``stdout`` in a nicely formatted message.

#     Args:
#       args (List[str]): command line parameters as list of strings
#           (for example  ``["--verbose", "42"]``).
#     """
#     args = parse_args(args)
#     setup_logging(args.loglevel)
#     _logger.debug("Starting crazy calculations...")
#     print(f"The {args.n}-th Fibonacci number is {fib(args.n)}")
#     _logger.info("Script ends here")


# def run():
#     """Calls :func:`main` passing the CLI arguments extracted from :obj:`sys.argv`

#     This function can be used as entry point to create console scripts with setuptools.
#     """
#     main(sys.argv[1:])


# if __name__ == "__main__":
#     # ^  This is a guard statement that will prevent the following code from
#     #    being executed in the case someone imports this file instead of
#     #    executing it as a script.
#     #    https://docs.python.org/3/library/__main__.html

#     # After installing your project with pip, users can also run your Python
#     # modules as scripts via the ``-m`` flag, as defined in PEP 338::
#     #
#     #     python -m birdshot.skeleton 42
#     #
#     run()
