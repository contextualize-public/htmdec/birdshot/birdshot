# NOTE: Any function code preceeding iteration through the stream will execute
# ACTIVELY (BEFORE) the stream is created. Therefore, actions that rely on
# state, such as accessing heap variables, must be done inside the
# `for obj in objs` construction.

import sys
import os
import click
import json
import logging
import networkx as nx
import pickle
import re
from . import Birdshot
from .pipe import generator, processor, params_to_dict
from ast import literal_eval
from click import UsageError
from mimetypes import guess_extension
from pathlib import Path
from pprint import pprint
from pydantic import BaseModel


# ##### Pipe operations
@click.argument(
    "input",
    type=Path,
    required=True)
@generator
def load(input):
    """
    Loads either a JSON or python pickle archive, based on the filename prefix.
    """
    loader = {
        ".json": json.load,
        ".pkl": pickle.load
    }.get(input.suffix, None)
    if loader is None:
        msg = f"{str(input)!r} ({input.suffix}) is not a recognized file format."
        logging.error(msg)
        raise IOError(msg)
    with open(input, "rb") as ifs:
        logging.debug(f"Loading {input!r}.")
        yield loader(ifs)


@click.argument(
    "prefix",
    type=Path,
    required=True)
@click.option(
    "--error",
    type=click.Choice([
        "exit",
        "stop",
        "skip"
    ], case_sensitive=False),
    default="exit",
    help="Exit, pass, or skip files that fail.")
@click.option(
    "-p", "--params",
    type=str,
    default="",
    help="String of function options in the format 'param1=value1|param2=value2|...'.")
@processor
def dump(objs, prefix, error, params):
    """
    Dumps the objects in the queue to archives. This attempts to write first to
    the more restrictive, but more ubiquitous, JSON archive and, if that fails,
    to the more general python pickle archive.

    Objects are consumed on dump, so if you wish to continue working on working
    with objects after they are saved, be sure to save them (push/pop) before
    writing.

    ## JSON Parameters

    \b
        skipkeys=False,
        ensure_ascii=True,
        check_circular=True,
        allow_nan=True,
        cls=None,
        indent=None,
        separators=None,
        default=None,
        sort_keys=False

    ## Pickle Parameters

    \b
        protocol=None,
        fix_imports=True,
        buffer_callback=None,
    """
    def get_filename(prefix, ext):
        ofile = f"{prefix}{ext}"
        count = 0
        while Path(ofile).exists():
            count += 1
            ofile = f"{basename}-{count}{ext}"
        return ofile
    
    basename, ext = os.path.splitext(prefix)
    for obj in objs:
        opts = params_to_dict(params) if params else dict()
        try:
            # try to dump as JSON
            jsonOpts = {k:v for k,v in opts.items() if k in (
                "skipkeys",
                "ensure_ascii",
                "check_circular",
                "allow_nan",
                "indent",
                "separators",
                "default",
                "sort_keys")}
            ofile = get_filename(basename, ".json")
            with open(ofile, "w") as ofs:
                json.dump(obj, ofs, **jsonOpts)
            logging.info(f"Object saved to {ofile!r}.")
        except:
            Path(ofile).unlink()
            logging.debug("Stream object not JSON serializable.")
        else:
            continue
        try:
            # try to dump as pickle
            pickleOpts = {k:v for k,v in opts.items() if k in (
                "protocol",
                "fix_imports",
                "buffer_callback"
            )}
            ofile = get_filename(basename, ".pkl")
            with open(ofile, "wb") as ofs:
                pickle.dump(obj, ofs, **pickleOpts)
            logging.info(f"Object saved to {ofile!r}.")
        except:
            Path(ofile).unlink()
            logging.debug("Stream object could not be pickled.")
            if error == "skip":
                continue
            elif error == "stop":
                break
            else:
                raise
    yield


@click.option(
    "--error",
    type=click.Choice([
        "exit",
        "stop",
        "skip"
    ], case_sensitive=False),
    default="exit",
    help="Exit, pass, or skip files that fail.")
@click.option(
    "-p", "--params",
    type=str,
    default="",
    help="String of function options in the format 'param1=value1|param2=value2|...'.")
@processor
def dumps(objs, error, params):
    """
    Dumps the objects to the stdout in JSON format.

    ## Parameters

    \b
        skipkeys=False,
        ensure_ascii=True,
        check_circular=True,
        allow_nan=True,
        cls=None,
        indent=None,
        separators=None,
        default=None,
        sort_keys=False
    """
    for obj in objs:
        opts = params_to_dict(params) if params else dict()
        try:
            click.echo(json.dumps(obj, **opts))
        except:
            logging.debug("Failed to dump object to JSON string.")
            if error == "stop":
                break
            elif error == "skip":
                continue
            else:
                raise
    yield


@click.option(
    "-k", "--key",
    type=str,
    required=True,
    help="Store the current stream.")
@click.option(
    "--squeeze",
    is_flag=True,
    default=True,
    help="If only one element is in the stream, store the element not a list.")
@processor
@click.pass_obj
def store(storage, objs, key, squeeze):
    """
    Store the arguments for future, keyed access.
    """
    args = list(objs)
    if squeeze:
        # len(args) does not evaluate. Call __len__method instead.
        storage._heap[key] = (args[0] if (args.__len__() == 1) else args)
    else:
        storage._heap[key] = args
    yield from args


@processor
def iter_(objs):
    """
    Iterates through stream objects that are iterable. Stream objects that are
    not iterable are passed through unaltered.
    """
    for obj in objs:
        if hasattr(obj, "__iter__") and not isinstance(obj, str):
            for x in obj:
                yield x
        else:
            yield obj


@click.option(
    "--pass", "pass_",
    is_flag=True,
    default=False,
    help="If set, the current stream will be stashed but also continue through the pipeline.")
@processor
@click.pass_obj
def push(storage, objs, pass_):
    """
    Pushes the objects onto the stack for future use.
    """
    args = list(objs)
    storage._stack.append(args)
    if pass_:
        for arg in args:
            yield arg


@click.option(
    "-i", "--index",
    type=int,
    required=False,
    default=-1,
    help="Index of the results to inject. Default: -1 (last `push`ed).")
@click.option(
    "-z", "--zip", "zip_",
    is_flag=True,
    default=False,
    help="`zip` stack contents (S) with existing arguments (A) into (a, s).")
@click.option(
    "-r", "--reverse",
    is_flag=True,
    default=False,
    help="Reverse the order of the stack (S) and existing arguments (A). Default: A then S.")
@processor
@click.pass_obj
def pop(storage, objs, index, zip_, reverse):
    """
    Adds the last set of objects pushed onto the stack. This is used in
    conjunction with `push` to temporarily store data for later processing.

    By default, the existing arguments are returned before the arguments popped
    from the stack. This can be reversed using the '-r/--reverse' flag.

    Existing and stack arguments can be zipped together ('-z/--zip' flag).
    Multiple calls to `pop` with construct an increasingly large tuple. For
    example:

    /b
        birdshot graph \\ # gets the BIRDSHOT graph
            field nodes \\ # gets the graph nodes iterator object
            iter \\ # iterates through the nodes
            push --pass \\ # pushes the nodes onto the stack for now and later
            field name \\ # gets the name field from each node
            push \\ # stores names in the stack for later
            pop --index 0 \\ # pops the nodes off the stack
            field id \\ # gets the id field from each node
            pop --zip # zips id and name into  (id, name) pairs.
    """
    def ensure_tuple(x):
        if not isinstance(x, tuple):
            return (x,)
        else:
            return x
        
    try:
        first, second = list(objs), storage._stack.pop(index)
        if reverse:
            first, second = second, first
    except IndexError:
        logging.warning("The stack is empty. Returning the object unaltered.")
        second = None
    if second is not None:
        if zip_:
            for a,b in zip(first, second):
                yield ensure_tuple(a) + ensure_tuple(b)
        else:
            yield from first
            yield from second
    else:
        yield objs


@processor
def drop(objs):
    """
    Drop all objects from the queue.
    """
    for _ in objs:
        pass
    yield from []


@processor
def len(objs):
    """
    Writes the length of the stream to the stdout.
    """
    count = 0
    for obj in objs:
        count += 1
        yield obj
    click.echo(count)


# ##### Container operations
@processor
def dict_(objs):
    """
    Typically used in conjunction with `pop --zip`, this generates a dictionary
    from a list of tuples.
    """
    yield dict(list(objs))


@processor
def list_(objs):
    """
    Convert the stream into a list.
    """
    yield [obj for obj in objs]


@click.argument(
    "field",
    type=str,
    required=True)
@click.option(
    "-m", "--missing",
    type=str,
    default="exit",
    help="Action for objects lacking the requested field: {exit|pass|skip|stop|<default>}.")
@processor
def field(objs, field, missing):
    """
    Extracts a field (obj.<FIELD>) from each object in the stream.
    """
    for obj in objs:
        try:
            yield getattr(obj, field)
        except AttributeError:
            if missing.lower() == "pass":
                yield obj
            elif missing.lower() == "skip":
                continue
            elif missing.lower() == "stop":
                break
            elif missing.lower() == "exit":
                raise
            else:
                try:
                    yield literal_eval(missing)
                except:
                    yield missing


@click.option(
    "-k", "--key",
    type=str,
    default=None,
    help="Dict key to access.")
@click.option(
    "-i", "--index",
    type=int,
    default=None,
    help="List index to access.")
@click.option(
    "-m", "--missing",
    type=str,
    default="exit",
    help="Action {exit|pass|skip|stop} or default value for objects lacking the requested field.")
@processor
def get(objs, key, index, missing):
    """
    Extracts a key (obj[<KEY>]) from each object in the stream.

    If not one of {exit, pass, skip, or stop}, a literal evaluation of the
    value is passed as the default, e.g. '--missing None'
    """
    for obj in objs:
        if index is None and key is None:
            raise click.UsageError("Either 'index' or 'key' must be provided.")
        if index is not None and key is not None:
            raise click.UsageError("Only one of 'index' or 'key' may be given.")
        idx = index if key is None else key
        try:
            yield obj[idx]
        except (IndexError, KeyError):
            # If (a) not indexable, (b) missing list index, (c) missing dict key
            if missing.lower() == "pass":
                yield obj
            elif missing.lower() == "skip":
                continue
            elif missing.lower() == "stop":
                break
            elif missing.lower() == "exit":
                raise
            else:
                try:
                    yield literal_eval(missing)
                except:
                    yield missing


@processor
def any_(objs):
    """
    Returns True if any enqueued value is True.
    """
    result = any(objs)
    if getattr(result, "__len__", lambda: -1)() == 1:
        yield result[0]
    else:
        yield result


@processor
def all_(objs):
    """
    Returns True if all enqueued values are True.
    """
    result = all(objs)
    if getattr(result, "__len__", lambda: -1)() == 1:
        yield result[0]
    else:
        yield result


@click.option(
    "-s", "--store",
    type=str,
    default=None,
    help="Retrieve the mask (objects must be castable to boolean) from a store variable.")
@click.option(
    "-i", "--index",
    type=int,
    default=-1,
    help="Retrieve the mask (objects must be castable to boolean) from the stack. Default: last stack entry.")
@click.option(
    "--push",
    is_flag=True,
    default=True,
    help="Push the mask back into the stack after extraction.")
@processor
@click.pass_obj
def where(storage, objs, store, index, push):
    """
    Select objects masked by the variable in "store" or the stack. By default
    the last entry in the stack is used. The shorter of mask and stream
    dictates the length of the output.
    """
    mask = None
    for obj in objs:
        if mask is None:
            # Set the mask
            if store is not None:
                # ... from the heap
                try:
                    mask = storage._heap[store]
                except KeyError:
                    raise KeyError(f"{store!r} is not accessible in the heap.")
            else:
                # ... from the stack
                try:
                    if push:
                        # ... push back into the stack (e.g., index)
                        mask = storage._stack[index]
                    else:
                        # ... pop from the stack
                        mask = storage._stack.pop(index)
                except IndexError:
                    raise IndexError("Cannot pop boolean mask from an empty stack.")
            maskIter = iter(mask)
        try:
            if bool(next(maskIter)):
                yield obj
        except StopIteration:
            break
        

@click.argument(
    "func",
    type=str,
    required=True)
@click.option(
    "-p", "--params",
    type=str,
    default="",
    help="String of function options in the format 'param1=value1|param2=value2|...'.")
@processor
@click.pass_obj
def apply(storage, objs, func, params):
    """
    For each iterable-like entry in the stream, apply the named function to
    each element in that iterable.

    Function parameters must be passed through the `params` option. The
    required arguments to each function can be found in the "help" of the
    function.
    """
    from functools import partial

    def is_iterable(x):
        return hasattr(x, "__iter__") and not isinstance(x, str)
    
    for obj in objs:
        if not is_iterable(obj):
            yield obj
        else:
            funcName = func.replace("-", "_") # CLI uses dashes; python, underscore
            fn = globals().get(funcName, globals().get(funcName + "_", None))
            if fn is None:
                raise UsageError(f"{func!r} is not a recognized function.")
            opts = params_to_dict(params) if params else dict()
            try:
                try:
                    # Function does not require access to storage.
                    result = list(fn(**opts)(iter(obj)))
                except TypeError as e:
                    click.echo(f"{func!r} (no storage) failed. Error: {str(e)}")
                    # Function does require access to storage.
                    result = list(fn(**opts)(storage, iter(obj)))
            except TypeError as e:
                raise UsageError(f"{func!r} is not a valid function for one " \
                                 f"of {set(type(o) for o in obj)} objects. Error: {str(e)}.")
            except Exception as e:
                # msg = f"{func}(iter({obj}), {opts}) failed with error {str(e)}."
                # raise type(e)(msg)
                # raise UsageError(str(e))
                raise
            yield result


# ##### Type operations
@processor
def bool_(objs):
    """
    Cast each object as a boolean.
    """
    for obj in objs:
        yield bool(obj)


@processor
def str_(objs):
    """
    Cast each object as a string.
    """
    for obj in objs:
        yield str(obj)


# ##### String operations
@click.argument("chars", type=str, required=True)
@processor
def strip(objs, chars):
    """
    Strips the chars from the front and back of strings.
    """
    for obj in objs:
        try:
            yield obj.strip(chars)
        except AttributeError:
            yield obj


@click.argument("old", type=str, required=True)
@click.argument("new", type=str, required=True)
@processor
def replace(objs, old, new):
    """
    Replace all occurances of <OLD> with <NEW>.
    """
    for obj in objs:
        try:
            yield obj.replace(old, new)
        except AttributeError:
            yield obj


@click.argument("pattern", type=str, required=True)
@click.option(
    "-i", "--ignore-case",
    is_flag=True,
    default=False,
    help="Perform a case-insensitive match.")
@click.option(
    "-m", "--missing",
    type=str,
    default="exit",
    help="Action for objects lacking the requested field: {exit|pass|skip|stop|<default>}.")
@processor
def match(objs, pattern, ignore_case, missing):
    """
    Performs a regular expression match between objects, treating the result as
    boolean.
    """
    for obj in objs:
        try:
            yield bool(re.match(pattern, obj, flags=re.IGNORECASE if ignore_case else 0))
        except:
            if missing.lower() == "exit":
                raise
            elif missing.lower() == "pass":
                yield obj
            elif missing.lower() == "skip":
                continue
            elif missing.lower() == "stop":
                break
            else:
                try:
                    yield literal_eval(missing)
                except:
                    yield missing


# ##### Operations on Birdshot.Entry and Graph objects
@generator
@click.pass_obj
def graph(storage):
    """
    Returns, and if necessary constructs from Google Drive, a graph of the
    BIRDSHOT data/filesystem.
    """
    if storage.birdshot:
        yield storage.birdshot.graph()
    else:
        yield


@processor
def nodes(objs):
    """
    Accesses the nodes from a stream.
    """
    for obj in objs:
        if isinstance(obj, nx.Graph):
            yield from obj.nodes
        elif isinstance(obj, Birdshot.Entry):
            yield obj


@click.option(
    "--id",
    multiple=True,
    default=[],
    help="Search records for matching ID.")
@click.option(
    "-n", "--name",
    multiple=True,
    default=[],
    help="Search for records with matching name.")
@click.option(
    "--default",
    type=str,
    default=None,
    help="Provides a default for objects that do not match or are not valid nodes.")
@processor
def node_filter(objs, id, name, default):
    """
    Search the BIRDSHOT Google Drive for entries matching the specified
    pattern. All patterns are interpreted as regular expressions.

    If multiple patterns are specified, the results are logically OR'ed. For
    example:

    \b
        birdshot graph nodes \\
            iter \\
            filter-records -n ".*.json" -".*.txt" \\
            filter-records -n "foo.*"

    will return all records matching ('.*.json' or '.*.txt') and ('foo.*'),
    i.e. any records with names matching 'foo.*.(json|txt)'.

    That is, arguments within a single filter command 'OR', subsequent filter
    commands 'AND'.

    All objects that are not records are dropped.
    """
    for obj in objs:
        matches = False
        if isinstance(obj, Birdshot.Entry):
            for pattern in id:
                if re.match(pattern, str(getattr(obj, "id", ""))):
                    matches = True
                    yield obj
            for pattern in name:
                if re.match(pattern, str(getattr(obj, "name", ""))):
                    matches = True
                    yield obj
        if (default is not None) and (not matches):
            yield default


@processor
def model_dump(objs):
    """
    Converts pydantic BaseModel-like objects to JSON. Other objects are
    returned unaltered.
    """
    for obj in objs:
        if isinstance(obj, BaseModel):
            yield obj.model_dump()
        else:
            yield obj


@click.argument(
    "filename",
    type=str,
    default=None,
    required=False)
@click.option(
    "-d", "--dest",
    type=Path,
    default=None,
    help="Directory where the downloaded file(s) should be stored.")
@processor
@click.pass_obj
def download(storage, objs, filename, dest):
    """
    Download the files associated with the Birdshot.Entry nodes.

    If specified, a FILENAME may be provided as a default. If not specified the
    files are saved to <node.name>-<node.id>.<node.suffix>. For example, for a
    node named "foo.txt" with id = "1234", the file will be saved to
    "foo-1234.txt".
    """
    def generate_filename(node):
        defaultExt = guess_extension(node.metadata["mimeType"]) or ""
        if filename:
            # A base filename was specified.
            count = 1
            fname = Path(filename)
            while fname.is_file():
                # Add an integer suffix to avoid overwriting files.
                name, ext = os.path.splitext(fname.name)
                ext = ext or defaultExt
                fname = Path(f"{name}-{count}{ext}")
                count += 1
        else:
            # Use the default filename.
            path = Path(node.name)
            name, ext = os.path.splitext(path)
            ext = ext or defaultExt
            fname = Path(f"{name}-{node.id}{ext}")
        return Path(dest).joinpath(fname) if dest else Path(fname)
    
    for obj in objs:
        # objects on the heap are stored as a list, even if there is only one
        # entry.
        if obj is None:
            continue
        path = generate_filename(obj)
        # path = Path(dest or ".").joinpath(name)
        try:
            with open(path, "wb") as ofs:
                ifs = storage.birdshot.read(obj.id)
                if ifs:
                    ofs.write(storage.birdshot.read(obj.id).getvalue())
                else:
                    msg = f"{obj.id!r} could not be read."
                    logging.error(msg)
                    raise IOError(msg)
        except:
            # cleanup the file if anything went wrong with the write.
            path.unlink()
            raise
    yield


@click.option(
    "--graph",
    type=str,
    default=None,
    help="Use a stored graph."
)
@processor
@click.pass_obj
def parents(storage, objs, graph):
    """
    Returns the parent folder(s) of each Birdshot.Entry node.

    If no parents are present, an empty list is returned.
    
    Note: Non-node entries in the stream are discarded.
    """
    for obj in objs:
        G = storage.birdshot.G if graph is None else storage._heap[graph]
        if isinstance(obj, Birdshot.Entry):
            yield list(G.predecessors(obj))


@click.option(
    "--graph",
    type=str,
    default=None,
    help="Use a stored graph."
)
@processor
@click.pass_obj
def children(storage, objs, graph):
    """
    Return the children nodes of each Birdshot.Entry node.

    If none, returns an empty list.

    Warning: All non-node objects are discarded.
    """
    for obj in objs:
        if isinstance(obj, Birdshot.Entry):
            G = storage.birdshot.G if graph is None else storage._heap[graph]
            yield list(G.successors(obj))


# ##### Filesystem-like operations
@click.option(
    "-g", "--graph",
    type=str,
    default=None,
    help="Key to access the previously stored graph.")
@processor
@click.pass_obj
def path(storage, objs, graph):
    """
    Returns the path to each Birdshot.Entry record. Multiple paths are not
    supported. 
    """
    def get_graph():
        if graph is None:
            logging.debug("Retrieving graph from Birdshot agent.")
            return storage.birdshot.G
        else:
            logging.debug(f"Retrieving graph from store[{graph!r}].")
            return storage._heap[graph]

    for obj in objs:
        # If not done inside the loop, the code will attempt to get the graph
        # before the storage is actually set (lazy loading).
        G = get_graph()
        if isinstance(obj, Birdshot.Entry):
            queue = [obj]
            parts = [obj]
            try:
                while True:
                    child = queue.pop()
                    logging.debug(f"Processing {getattr(child, 'name', 'None')!r}.")
                    try:
                        parent = list(G.predecessors(child))[0]
                    except IndexError:
                        continue
                    except Exception as e:
                        msg = f"Child: ({getattr(child, 'id', 'None')}, {getattr(child, 'name', 'None')}, {type(child)}, {child})"
                        logging.error(msg)
                        raise type(e)(msg)
                    else:
                        queue.append(parent)
                        parts.append(parent)
            except IndexError:
                pass
            sep = os.path.sep
            yield sep + sep.join([n.name for n in reversed(parts)])
