import click
import logging
from ast import literal_eval
from functools import update_wrapper
from typing import Dict


def processor(f):
    """Creates a processor from a function."""
    _funcName = getattr(f, "__name__", "function")
    def new_func(*args, **kwargs):
        def processor(stream):
            try:
                result = f(stream, *args, **kwargs)
                logging.debug(f"{_funcName!r} completed successfully.")
                return result
            except Exception as e:
                logging.error(f"{_funcName!r} failed with message: {str(e)}.")
                raise
        return processor
    logging.debug(f"Processor produced from {_funcName!r}.")
    return update_wrapper(new_func, f)


def generator(f):
    """Creates a generator from a function."""
    _funcName = getattr(f, "__name__", "function")
    @processor
    def new_func(stream, *args, **kwargs):
        yield from stream
        yield from f(*args, **kwargs)
    logging.debug(f"Generator produced from {_funcName!r}.")
    return update_wrapper(new_func, f)


def params_to_dict(arg: str) -> Dict:
    """
    Parse a string of the form "param1=value1|param2=value2|..." into a dict.
    """
    params_asdict = {}
    if arg:
        params = arg.split("|")
        for p in params:
            opts = p.split("=")
            try:
                # try to interpret the string for instance "True" -> True
                params_asdict[opts[0]] = literal_eval(opts[1])
            except Exception:
                # fallback to string
                params_asdict[opts[0]] = opts[1]
    # As these are being passed as streams, do not allow 'inplace' operations
    params_asdict.pop("inplace", None)
    return params_asdict
